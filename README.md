nau-cas
=====================

NAU specific implementations of a Passport Strategy and AuthN / AuthZ middleware for CAS.


## Initialization

The module can be used as either a singleton (preferred) or you can instantiate the class directly.

To initialize the singleton, require the module, and pass it your local passport instance and your configuration:

```javascript
var naucas = require('nau-cas'),
    passport = require(passport),
    config = { ... };

CAS = nau(passport, config); // returns the singleton instance.
```

Each subsequent call to `require('nau-cas')()` will return the same instance. Note the empty function call at the end of the require.


## Config

Your configuration object can contain any of the following keys:

- `serviceBaseUrl` : (**required**) The base URL of your application. This is the only required configuration parameter, eg. "http://localhost:3000".
- `casServiceUrl` : URL to access CAS. If set, it will also set *casServiceUrlBase*, *casServiceUrlPath*. If not set, it will be built at runtime from *casServiceUrlBase* and *casServiceUrlPath*.
- `casServiceUrlBase` : The base CAS URL, containing protocol and host only. Default: "https://cas.nau.edu"
- `casServiceUrlPath` : The path at which to access CAS services. Default: "/cas"
- `authz` : An object containing AuthZ configuration or an AuthZ adapter that as been previously initialized.
  - `adapter` : The name of the AuthZ adapter to use.
  - `groups`: (*string array*) NAU Enterprise groups that should be checked for user membership. If set, after successful authentication with CAS, the middleware will query the AuthZ server for the user's group membership for each group name in the array, eg. ["Users", "Administrators"]. The user object set by passport (req.user) will have an additional property set (see below) which will contain an array of group names that the user has access to, eg. ["Users"], or an empty array if not authorized for any group.
  - `groupsRequireAll`: (*boolean*) If truthy, all groups will be matched for AuthZ, otherwise any matched group will pass. Default: false
  - `groupsUserSave`: (*boolean*) If truthy, the middleware will attempt to call `.save()` on the user object after setting the groups array. This will work as expected for Mongoose record objects. Default: false
  - `groupsUserProperty`: The name of the property on the user object to set the group array. Default: "roles"
  - `...` : More options specific to the adapter. See below.
- `passReqToCallback` : Passes the request on to the original URL. Default: true
- `postRedirect` : (*boolean*) Passes the request as a POST. Default: false
- `name` : The name of the authentication strategy. This only needs to be set if you have more than one instance of the strategy at a time.
- `debug` : (*boolean*|*function*) If truthy, extended debug information will be shown on the console. If a function is passed in, debug messages will be passed individually to the function and can be passed to the console or another log as desired.
- `isAuthenticated` : (*function*) Is passed the request object from the middleware and should return true if the current user is authenticated. Defaults to the value of `req.isAuthenticated()`, but can be overriden to add additional logic.

In general, if you are using the standard NAU CAS system, you will only need to specify *serviceBaseUrl* and *casAuthzGroups* / *casAuthzGroupsUserSave* in your configuration. In dev and test environments, you should also set *casServiceUrlBase* to "https://cas-test.nau.edu".


### AuthZ Configuration

#### LDAP AuthZ

This is the preferred group AuthZ method.

- `url` : The LDAP access URL. Default: "ldaps://ldap.nau.edu"
- `dn` : The base DN to use for searches. Default: "ou=people,dc=nau,dc=edu"
- `cn` : (*required*) The CN for the application authorized to make group queries. Will be built automatically from *app* if not set.
- `app` : Required if *cn* is not set. The name of the application, the cn will be built using: "cn=[YOUR APP NAME],ou=Applications,dc=nau,dc=edu"
- `password` : (*required*) Application password

#### CAS AuthZ

*NOTE:* This adapter will no longer work soon. It needs a refactor to work with CAS 4.

- `url` : URL to access AuthZ services. If not set, it will be built at runtime.
- `path` : The path at which to access AuthZ services. Default: "/NAUauthZ/NAUauthZ"
- `validateMethod` : The method used to validate the CAS result. Default: "serviceValidate"
- `validateUri` : The URL at which to validate the CAS result. Default: "/serviceValidate"


### Example Configuration:
```javascript
  cas: {
    serviceBaseUrl: 'https://localhost:1337/app',
    authz: {
      adapter: 'cas',
      groups: [
        'ABCDEF-maintenance-Users',
        'ABCDEF-maintenanceReadOnly-Users'
      ],
      groupsUserSave: true
    }
  }
```

## Passport Strategy

The passport strategy works like most others. After initializing the CAS module, call `useStrategy([options,] verifyCallback)`. The optional first paramter will override currently set options on the instance. The second parameter is a standard Passport verification callback and is required.


```javascript
var CAS = require('nau-cas')();

CAS.useStrategy(function(req, data, done) {
  var ticket = req.query.ticket;

  if (data && data.success) {
    data.data['cas:ticket'] = ticket;

    // Find and update or create the user
    User.findOne({
      username: data.user
    }, function(err, user) {
      if (err) {
        return done(err);
      }

      // Save (and/or create) the user
      if (!user) {
        user = new User();
      }

      user.username = data.user;
      user.provider = 'cas';
      user.providerData = data.data;

      user.save(function (err) {
        if (err) {
          done(err);
        }

        done(null, user);
      });
    });
  } else {
    done(null, false);
  }
});
```


## Middleware

The module's Express.js middleware is even easier to use. Simply get the middleware function from the module after initializing it, and use it to secure routes.

### AuthN

```javascript
var requireCasLogin = require('nau-cas').getAuthenticationMiddleware()

app.route('/').get(requireCasLogin, function (req, res) {
    // User is logged in and available as req.user
    ...
})
```

### AuthZ

If you have defined groups in the global settings, AuthZ middleware works just like AuthN.

```javascript
var authz = require('nau-cas').getAuthorizationMiddleware()

app.route('/').get(authz, function (req, res) {
    // User is logged in and has authorization
    ...
})
```

If you need to authorize a specific group or set of groups, set req.authzGroups and call the middleware.
The user is authorized if any of the groups matches.

```javascript
var authz = require('nau-cas').getAuthorizationMiddleware()

app.route('/').get(
  function (req, res, next) {
    req.authzGroups = ['MyGroup1', 'MyGroup2']
  },
  authz,
  function (req, res) {
    // User is logged in and has authorization
    ...
  }
)
```

For more complicated AuthZ scenarios, you may need to initialize an AuthZ adapter directly from the
lib/authz module.

fin.
