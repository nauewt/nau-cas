// Generated by CoffeeScript 1.10.0
var AuthzAbstract, _, dump;

_ = require("lodash");

dump = function(obj) {
  return console.log((require("util")).inspect(obj, {
    depth: null,
    colors: true
  }));
};

AuthzAbstract = (function() {
  function AuthzAbstract(options) {
    this.options = {
      debug: function() {},
      groups: null
    };
    this.set(options);
  }

  AuthzAbstract.prototype.set = function(options) {
    var dumper;
    if (!options) {
      return;
    }
    if ("debug" in options) {
      dumper = typeof options.debug === "function" ? options.debug : dump;
      options.debug = options.debug ? function() {
        (_.toArray(arguments)).map(dumper);
        return console.log("");
      } : function() {};
    }
    return this.options = _.merge({}, this.options, options);
  };

  AuthzAbstract.prototype.isValid = function() {
    return true;
  };

  AuthzAbstract.prototype.authorize = function(uid, groups, all) {};

  AuthzAbstract.prototype.authorizeAll = function(uid, groups) {
    return this.authorize(uid, groups, true);
  };

  return AuthzAbstract;

})();

AuthzAbstract.dump = dump;

module.exports = AuthzAbstract;
