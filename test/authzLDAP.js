'use strict'

var should = require('should')
var Promise = require('bluebird')

// var cas = require('../lib')
// var AuthzCAS = require('../lib/authz').AuthzCAS
var AuthzLDAP = require('../lib/authz').AuthzLDAP

console.log('--> Beginning AuthzLDAP Tests\n')

var authz = new AuthzLDAP({
      debug: false,
      app: 'Orientation Registration Web App',
      password: null
    })

// Single group test
Promise.all([
  authz.authorize('jwh9', 'Non-Existant-Group')
    .then(function (authorized) {
      console.log('Test: single group unauthorized: ', authorized)

      should.exist(authorized)
      authorized.should.equal(false)
      console.log('\t...Pass!')
    })
    .catch(function (err) {
      console.error('Single group unauthorized failed: ' + err && err.message || err)
    }),

  authz.authorize('jwh9', 'ITS')
    .then(function (authorized) {
      console.log('Test: single group authorized: ', authorized)

      should.exist(authorized)
      authorized.should.not.equal(false)
      authorized.should.have.lengthOf(1)
      authorized[0].should.equal('ITS')
      console.log('\t...Pass!')
    })
    .catch(function (err) {
      console.error('Single group authorized failed: ' + err && err.message || err)
    }),

  authz.authorize('jwh9', ['ITS', 'ITS-EIS-EWT'], true)
    .then(function (authorized) {
      console.log('Test: All groups "AND" authorized: ', authorized)

      should.exist(authorized)
      authorized.should.not.equal(false)
      authorized.should.have.lengthOf(2)
      authorized[0].should.equal('ITS')
      authorized[1].should.equal('ITS-EIS-EWT')
      console.log('\t...Pass!')
    })
    .catch(function (err) {
      console.error('All groups "AND" authorized: ' + err && err.message || err)
    }),

  authz.authorize('jwh9', ['ITS', 'ITS-EIS-EWT', 'Non-Existant-Group'], true)
    .then(function (authorized) {
      console.log('Test: All groups "AND" unauthorized: ', authorized)

      should.exist(authorized)
      authorized.should.equal(false)
      console.log('\t...Pass!')
    })
    .catch(function (err) {
      console.error('All groups "AND" unauthorized: ' + err && err.message || err)
    }),

  authz.authorize('jwh9', ['ITS', 'ITS-EIS-EWT', 'Non-Existant-Group'], false)
    .then(function (authorized) {
      console.log('Test: Any group "OR" authorized: ', authorized)

      should.exist(authorized)
      authorized.should.not.equal(false)
      authorized.should.have.lengthOf(2)
      authorized[0].should.equal('ITS')
      authorized[1].should.equal('ITS-EIS-EWT')
      console.log('\t...Pass!')
    })
    .catch(function (err) {
      console.error('Any group "OR" authorized: ' + err && err.message || err)
    })

])
.then(function (results) {
  console.log('\n<-- All done with AuthzLDAP!\n')
  process.exit(0)
})
