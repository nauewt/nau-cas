_ = require "lodash"
async = require "async"
request = require "request"
{Strategy} = require "./strategy"
{AuthzAbstract, AuthzCAS, AuthzLDAP, AuthzLDERP} = require "./authz"


dump = (obj) -> (console.log ((require "util").inspect obj, { depth: null, colors: yes }))


class CAS
  constructor: (passport, options) ->
    throw (Error "passport object required") unless passport

    @passport = passport
    @authzAdapter = null
    @options =
      debug: ->
      name: "nau-cas"
      casServiceUrl: null
      casServiceUrlBase: "https://cas.nau.edu"
      casServiceUrlPath: "/cas"
      serviceBaseUrl: null
      validateUri: "/serviceValidate"
      validateMethod: "serviceValidate"
      passReqToCallback: true
      postRedirect: no
      authz: {
        adapter: null,
        groups: null,
        groupsRequireAll: no
        groupsUserSave: no
        groupsUserProperty: "roles"
      }
      isAuthenticated: (req) -> req.isAuthenticated()

    @set options


  set: (options) ->
    return unless options

    if "debug" of options
      dumper = if typeof options.debug is "function" then options.debug else dump
      options.debug =
        if options.debug then ->
          (_.toArray arguments).map dumper
          console.log ""
        else ->

    @options = _.merge {}, @options, options

    # parse specified url (service url takes priority)
    if @options.casServiceUrl
      {protocol, host, path} = (require "url").parse @options.casServiceUrl
      @options.casServiceUrlBase = "#{protocol}//#{host}"
      @options.casServiceUrlPath = path

    # Use default URL components unless specified
    @options.casServiceUrl ?= @options.casServiceUrlBase + @options.casServiceUrlPath

    if @options.authz instanceof AuthzAbstract
      @authzAdapter = @options.authz
    else if @options.authz?.adapter?
      @options.authz.adapter = (String @options.authz.adapter).toUpperCase()
      # @options.authz.debug = @options.debug

      if @options.authz.adapter is "CAS"
        @authzAdapter = new AuthzCAS (_.omit @options.authz, "adapter")
      else if @options.authz.adapter is "LDAP"
        @authzAdapter = new AuthzLDAP (_.omit @options.authz, "adapter")
      else if @options.authz.adapter is "LDERP"
        @authzAdapter = new AuthzLDAP (_.omit @options.authz, "adapter")

    @options.debug "NAU CAS options set", @options

    return @options


  useStrategy: (options, verifyCallback) ->
    if typeof options is "function"
      verifyCallback = options
      options = {}

    @set options
    return @passport.use new Strategy @options, verifyCallback

  # CAS Authenitcation middleware (Authn)
  authenticate: (req, res, next) ->
    if @options.isAuthenticated(req)
      @options.debug "NAU CAS User logged in: ", req.user
      next()
    else
      @options.debug "NAU CAS User not logged in, redirect to CAS."
      return (@passport.authenticate @options.name) req, res, =>
        ticket = req.query.ticket
        delete req.query.ticket

        query = (k + "=" + req.query[k] for k of req.query).join "&"

        returnUrl = req.baseUrl + req.path
        returnUrl = returnUrl + "?" + query if query.length

        # Check AuthZ groups if requested
        @authorize req, res, =>
          res.redirect returnUrl


  authorize: (req, res, next) ->
    next (Error "Cannot authorize, NAU CAS User not logged in") unless req.user

    uid = req.user?.providerData?["cas:user"] or req.user?.uid or req.user?.username
    groups = req.authzGroups or @options.authz?.groups or []
    groups = [groups] unless _.isArray groups

    @options.debug "Attempting to authorize NAU CAS User", uid, groups

    if @authzAdapter? and groups
      (@authzAdapter.authorize uid, groups, @options.authz.groupsRequireAll)
      .then (authorized) =>
        # authorized will either be false
        unless authorized
          (res.status 403).send { error: 'Unauthorized' }
          return
        # or an array of matched groups
        req.user[@options.authz.groupsUserProperty] = authorized
        req.user.save?() if @options.authz.groupsUserSave
        next()
      .catch (err) ->
        (res.status 400).send { error: err?.message or err }
    else
      next()

# Singleton and helpers
instance = null

module.exports = exports = (passport, options) ->
  instance = new CAS passport, options unless instance
  return instance

exports.CAS = CAS

exports.getInstance = -> return instance

exports.useStrategy = (options, verify) ->
  return instance.useStrategy options, verify


# Deferred versions of instance methods
exports.getAuthenticationMiddleware = (options) ->
  return (req, res, next) ->
    if instance
      instance.set options
      instance.authenticate req, res, next
    else
      next Error "NAU CAS not initialized"

exports.getMiddleware = exports.getAuthenticationMiddleware

exports.getAuthorizationMiddleware = (options) ->
  return (req, res, next) ->
    if instance
      instance.set options
      instance.authorize req, res, next
    else
      next Error "NAU CAS not initialized"

###
  cas: {
    casURL: "https://cas-test.nau.edu/cas",
    validateURL: "/serviceValidate",
    serviceURL: "http://134.114.169.70:10036/app",
    authz: {
      url: "https://cas-test.nau.edu/NAUauthZ/NAUauthZ",
      groups: [
        "ENSRV-admisMaintenance-Users", // Admin, Read/Write
        "ENSRV-admisMaintenanceReadOnly-Users" // Admin, Read only
      ]
    }
  },
###
