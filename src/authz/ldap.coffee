_ = require "lodash"
Promise = require "bluebird"
ldap = require "ldapjs"

AuthzAbstract = require "./abstract"

class AuthzLDAP extends AuthzAbstract
  constructor: (options) ->
    @options =
      debug: ->
      groups: null
      url: "ldaps://ldap.nau.edu"
      dn: "ou=people,dc=nau,dc=edu"
      cn: null
      password: null

    @set options


  set: (options) ->
    return unless options

    if options.app
      options.cn = options.app
      delete options.app

    super

    @options = _.merge {}, @options, options

    if @options.cn and not /^cn=/i.test @options.cn
      @options.cn = "cn=#{ @options.cn },ou=Applications,dc=nau,dc=edu"

    # @options.debug "LDAPauthZ options set", @options
    @options


  isValid: ->
    return @options.cn and @options.password


  authorize: (uid, groups, all) ->

    groups ||= @options.groups
    groups = [groups] unless _.isArray groups

    (new Promise (resolve, reject) =>
      reject Error "Application cn and password are required" unless @isValid()

      client = ldap.createClient
        url: @options.url
        tlsOptions:
          rejectUnauthorized: no
          secureProtocol: "SSLv23_method"
          minDHSize: 1

      client.bind @options.cn, @options.password, (err) =>
        reject err if err

        filter = @buildFilter uid, groups, all
        entries = []

        @options.debug "LDAPauthZ filter", filter

        client.search @options.dn, { scope: "sub", filter: filter }, (err, res) ->
          reject err if err

          res.on 'searchEntry', (entry) -> entries.push entry.object
          res.on 'searchReference', (referral) ->
            console.log 'referral: ' + referral.uris.join()
          res.on 'error', (err) -> reject err
          res.on 'end', (result) -> resolve entries

    ).then (entries) =>
      #@options.debug "LDAPauthZ found entries", entries
      authorized = no
      authorized = (_.intersection groups, ([].concat entries[0].nauEduMemberOf)) if entries[0] and entries[0].nauEduMemberOf 
      @options.debug "LDAPauthZ authorized roles", authorized
      authorized

  buildFilter: (uid, groups, all) ->
    # (&(uid=jwh9)(|(nauedumemberof=ENSRV-admisMaintenance-Users)(nauedumemberof=ENSRV-admisMaintenanceReadOnly-Users)))
    groups = ("(nauedumemberof=#{group})" for group in groups).join ""
    groups = if all then "(&#{groups})" else "(|#{groups})"
    "(&(uid=#{uid})#{groups})"


module.exports = AuthzLDAP

###
exports.getMiddleware = (options) ->
  return (req, res, next) ->
    if instance
      (instance.getMiddleware options) req, res, next
    else
      next (Error "NAU CAS not initialized")


  cas: {
    casURL: "https://cas-test.nau.edu/cas",
    validateURL: "/serviceValidate",
    serviceURL: "http://134.114.169.70:10036/app",
    authz: {
      url: "https://cas-test.nau.edu/LDAPauthZ/LDAPauthZ",
      groups: [
        "ENSRV-admisMaintenance-Users", // Admin, Read/Write
        "ENSRV-admisMaintenanceReadOnly-Users" // Admin, Read only
      ]
    }
  },
###
