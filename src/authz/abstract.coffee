_ = require "lodash"

dump = (obj) -> (console.log ((require "util").inspect obj, { depth: null, colors: yes }))

class AuthzAbstract
  constructor: (options) ->
    @options =
      debug: ->
      groups: null

    @set options

  set: (options) ->
    return unless options

    if "debug" of options
      dumper = if typeof options.debug is "function" then options.debug else dump
      options.debug =
        if options.debug then ->
          (_.toArray arguments).map dumper
          console.log ""
        else ->

    @options = _.merge {}, @options, options

  isValid: -> yes
  authorize: (uid, groups, all) ->
  authorizeAll: (uid, groups) -> @authorize(uid, groups, yes)

AuthzAbstract.dump = dump

module.exports = AuthzAbstract
