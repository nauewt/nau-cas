_ = require "lodash"
Promise = require "bluebird"
request = require "request"
XML = require 'pixl-xml'
AuthzAbstract = require "./abstract"

class AuthzCAS extends AuthzAbstract
  constructor: (options) ->
    @options =
      debug: ->
      groups: null
      url: "https://cas.nau.edu"
      path: "/NAUauthZ/NAUauthZ"

    @set options


  set: (options) ->
    return unless options

    super

    # parse specified url (service url takes priority)
    if @options.url or @options.path
      {protocol, host, path} = (require "url").parse @options.url
      @options.url = "#{protocol}//#{host}"
      @options.path = options.path or path or @options.path

    # @options.debug "NAUauthZ options set", @options
    @options


  authorize: (uid, groups, all) ->
    groups ||= @options.groups
    groups = [groups] unless _.isArray groups

    Promise.all (@authorizeUserByGroup uid, group for group in groups)
    .then (authorized) =>
      authorized = authorized.filter (group) -> !!group
      @options.debug "NAUauthZ authorized roles", authorized

      required = if all then groups.length else 1
      if authorized.length >= required then authorized else no


  # TODO: This will need to be refactored to use PGT/Proxy tickets in order to work with CAS 4
  authorizeUserByGroup: (uid, group) ->
    authxml = XML.stringify
      uid: uid
      casproxyticket: req.user.providerData["cas:ticket"]
      clientip: req.ip
      casservicename: @options.serviceBaseUrl + req.path
      nauauthz:
        group:
          name: "access_group"
          value: group
        logic: "(access_group)"
    , 'document'

    @options.debug "NAU CAS checking authz for group #{ group }"

    new Promise (resolve, reject) =>

      request @options.authzCasUrl + "?authxml=" + encodeURIComponent(authxml), (err, response, body) ->
        reject err or "server error" if err or response.statusCode isnt 200

        body = XML.parse body, { lowerCase: no }
        resolve body.nauauthzResponse?.authenticationSuccess?


module.exports = AuthzCAS

###
exports.getMiddleware = (options) ->
  return (req, res, next) ->
    if instance
      (instance.getMiddleware options) req, res, next
    else
      next (Error "NAU CAS not initialized")


  cas: {
    casURL: "https://cas-test.nau.edu/cas",
    validateURL: "/serviceValidate",
    serviceURL: "http://134.114.169.70:10036/app",
    authz: {
      url: "https://cas-test.nau.edu/NAUauthZ/NAUauthZ",
      groups: [
        "ENSRV-admisMaintenance-Users", // Admin, Read/Write
        "ENSRV-admisMaintenanceReadOnly-Users" // Admin, Read only
      ]
    }
  },
###
