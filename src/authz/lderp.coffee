_ = require "lodash"
Promise = require "bluebird"

AuthzAbstract = require "./abstract"

class AuthzLDERP extends AuthzAbstract
  constructor: (options) ->
    @options =
      debug: ->
      lderp: null
      groups: null
      cn: null
      password: null

    @set options


  set: (options) ->
    return unless options

    if options.app
      options.cn = options.app
      delete options.app

    super

    @options = _.merge {}, @options, options
    @options


  isValid: ->
    return @options.derp and @options.cn and @options.password


  authorize: (uid, groups, all) ->

    groups ||= @options.groups
    groups = [groups] unless _.isArray groups

    (new Promise (resolve, reject) =>
      # reject Error "LDERP command, application cn, and password are required" unless @isValid()

      command = "#{ @options.lderp } --cn=\"#{ @options.cn }\" --password=\"#{ @options.password }\" --uid=\"#{ uid }\" " + (("\"#{ group }\"" for group in groups).join ' ')
      # command = command + ' -d' if @options.debug

      @options.debug command

      (require "child_process").exec command,
        env: process.env
      , (error, stdout, stderr) =>
        @options.debug stdout
        return if (error or stderr) then (reject error or stderr) else resolve stdout

    ).then (matched) =>
      authorized = no
      authorized = (_.intersection groups, ([].concat (JSON.parse matched))) if matched
      @options.debug "LDERPauthZ authorized roles", authorized
      authorized

module.exports = AuthzLDERP
